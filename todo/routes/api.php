<?php

use App\Http\Controllers\TodoController;
use Illuminate\Support\Facades\Route;

Route::get('todos', [TodoController::class, 'index']);

Route::post('saveTodo', [TodoController::class, 'saveTodo']);

Route::post('deleteTodo', [TodoController::class, 'deleteTodo']);

Route::post('markDone', [TodoController::class, 'markDone']);