<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TodoController extends Controller
{

   public function index(Request $request)
   {
      return Todo::latest()->get();
   }

   public function saveTodo(Request $request)
   {
      Log::info('request', $request->all());
      $todo = new Todo();
      $todo->text = $request->get('text');
      $todo->save();

      return response()->json([
         'message' => "saved"
      ]);
   }

   public function deleteTodo(Request $request)
   {
      $todo = Todo::find($request->get('id'));
      $todo->delete();

      return response()->json([
         'message' => "todo deleted"
      ]);
   }

   public function markDone(Request $request)
   {
      $todo = Todo::find($request->get('id'));
      $todo->done = true;
      $todo->update();

      return response()->json([
         'message' => "todo updated"
      ]);
   }
}
